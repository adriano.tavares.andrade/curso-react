// Teste 1

// import React, {Component} from 'react'

// // Exercício 1
// const BemVindo = (props) => {
//   return (
//     <div>
//       <h2>Bem-vindo(a) {props.nome} </h2>
//       <h3>Tenho {props.idade} anos</h3>
//       <hr/>
//     </div>
//   )
// }

// // Exercício 2
// const Equipe = (props) => {
//   return (
//     <div>
//       <Sobre nome={props.nome} idade={props.idade} cargo={props.cargo}/>
//       <Social link={props.link}/>
//       <hr/>
//     </div>
//   )
// }

// const Sobre = (props) => {
//   return (
//     <div>
//       <h2>Meu nome é {props.nome}</h2>
//       <h2>Tenho {props.idade} anos</h2>
//       <h2>Meu cargo é {props.cargo}</h2>
//     </div>
//   )
// }

// const Social = (props) => {
//   return(
//     <div>
//       <a href={props.link}>Facebook </a>
//       <a href={props.link}>Linkedin </a>
//       <a href={props.link}>Youtube</a>
//     </div>
//   )
// }

// // Exercício 3
// class Teste extends Component{
//   render(){
//     return(
//       <div>
//         <h2>{this.props.nome}</h2>
//         <hr/>
//       </div>
//     )
//   }
// }

// // Método principal
// function App(){
//   return (
//     <div>
//       <h1>Olá mundo!</h1>
//       <BemVindo nome="Adriano" idade="22"/>
//       <BemVindo nome="Laiz" idade="21"/>
//       <Equipe nome="Yuri" idade="24" cargo="RH" link="https://google.com"/>
//       <Equipe nome="Matheus" idade="23" cargo="CEO do Sexo" link="https://google.com"/>
//       <Teste nome="Teste"/>
//     </div>
//   )
// }

// export default App

// Teste 2

import React, {Component} from 'react'

class App extends Component{

  constructor(props){
    super(props)
    this.state = {
      nome:"Adriano",
      cont:0,
      hora: '00:00:00'
    }
    
    this.diminuir = this.diminuir.bind(this)
    this.aumentar = this.aumentar.bind(this)
  }

  diminuir(){
    if(this.state.cont > 0){
      let state = this.state
      state.cont--
      this.setState(state)
      console.log("Diminuiu!")
    }
  }

  aumentar(){
    if(this.state.cont < 10){
      let state = this.state
      state.cont++
      this.setState(state)
      console.log("Aumentou!")
    }
  }

  componentDidMount(){
    setInterval( () => {
      this.setState( {hora:new Date().toLocaleDateString()} )
    },1000)
  }

  componentDidUpdate(){
    console.log("Atualizou")
  }

  /*
  shouldComponentUpdate(){

  }
  */

  render(){
    return(
      <div>
        <h1>Contador</h1>
        <h2>
          <button onClick={this.diminuir}>-</button>
          {this.state.cont}
          <button onClick={this.aumentar}>+</button>
        </h2>
        <h3>Meu projeto {this.state.hora}</h3>
      </div>
    )
  }
}

export default App