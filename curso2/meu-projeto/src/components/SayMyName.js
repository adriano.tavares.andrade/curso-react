export default function SayMyName(props){
    return (
        <div>
            <p>Olá, {props.nome}!</p>
        </div>
    )
}