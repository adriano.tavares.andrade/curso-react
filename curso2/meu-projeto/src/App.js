import './App.css'
import {useState} from 'react'

import HelloWorld from './components/HelloWorld'
import SayMyName from './components/SayMyName'
import Pessoa from './components/Pessoa'
import Carro from './components/Carro'
import List from './components/List'
import Evento from './components/Evento'
import Form from './components/Form'
import Condicional from './components/Condicional'
import OutraLista from './components/OutraLista'
import SeuNome from './components/SeuNome'
import Saudacao from './components/Saudacao'

function App() {

  const nome = "Yuri";
  const meusItens = ['React','Vue','Angular']

  {/* state de SeuNome.js deve ser definido no elemento pai */}
  const [name,setName] = useState()

  return (
    <div className="App">
      {/* olá mundo */}
      <HelloWorld/>

      {/* props, destructuring */}
      <SayMyName nome="Adriano"/>
      <SayMyName nome={nome}/>
      <Pessoa 
        nome="Adriano" 
        idade="22" 
        profissao="Programador" 
        foto="https://via.placeholder.com/150"
      />
      <Carro nome="Ford" cor="Azul"/>

      {/* fragments, component dentro de component, tipos de prop, valor padrão */}
      <List/>

      {/* eventos, state */}
      <h1>Testando eventos</h1>
      <Evento/>
      <Form/>

      {/* condição */}
      <Condicional/>

      {/* listas */}
      <OutraLista itens={meusItens}/>
      <OutraLista itens={[]}/>

      {/* state lift */}
      <SeuNome setName={setName}/>
      <Saudacao name={name}/>


    </div>
  )

}

export default App;
